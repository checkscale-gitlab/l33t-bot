package impl

import (
	"telegram/api"
	"time"
)

// ApiEndpoint - Global Telegram common API endpoint for all requests
const ApiEndpoint string = "https://api.telegram.org/bot"

// GetUpdatesPath - appendix for ApiEndpoint for GetUpdates call
const GetUpdatesPath = "/getUpdates"

// GetChatPath - appendix for ApiEndpoint for GetChat call
const GetChatPath = "/getChat"

const GetChatMemberPath = "/getChatMember"
const SendMessagePath = "/sendMessage"
const DeleteMessagePath = "/deleteMessage"

// PollingInterval - polling interval between bot update lookups
const PollingInterval = time.Millisecond * 300

// BotToken - bot token as given by Telegram @BotFather. Filled in main()
var BotToken string

type CaughtUrl struct {
	// text that was catched from URL
	Text string

	// chat where this occurred
	Chat *api.Chat
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}
