package impl

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"sync"
	"telegram/api"
	"time"
	"unicode/utf8"

	"github.com/bradfitz/latlong"
)

var /* const */ LEET_REGEX = []*regexp.Regexp{
	regexp.MustCompile("0000(00|0)?"),             // more is pointless, there is no 00 day or month or year
	regexp.MustCompile("0123(45|4)?"),             // 6789 are pointless, we can match only HHmmss
	regexp.MustCompile("1111(111111|1111|11|1)?"), // so max 1111111111 matches MMddHHmmss
	regexp.MustCompile("1234(56|5)?"),             // 789 are pointless, see #2
	regexp.MustCompile("1337"),                    // best regexp, truly
	regexp.MustCompile("2222(2222|22|2)?"),        // so max 22222222 matches ddHHmmss
	regexp.MustCompile("(01)?2345"),               // first day of month - 012345 matches ddHHmm
}

// Poller for bot updates
// repeatedly retrieves updates from the telegram server and updates its read position
// Note: runs in its own thread
type Poller struct {
	client       TelegramRestClient
	lastId       int64             // last Telegram update ID we know
	narrateAhead bool              // yes if we're already waiting to print summary, false otherwise
	db           *PersistenceLayer // database
	scoresToday  []Score
	today        time.Time
}

// starts poller
func (poll *Poller) Start(waiter *sync.WaitGroup) {
	// init
	poll.client = &BotRestClient{http.Client{Timeout: time.Second * 5}}
	ticker := time.NewTicker(PollingInterval)
	poll.db = newDb("data/scores.db")
	poll.today = time.Unix(0, 0)

	go poll.doWork(ticker, waiter)
}

func (poll *Poller) doWork(ticker *time.Ticker, waiter *sync.WaitGroup) {
	defer poll.db.Close()
	defer waiter.Done()

	for tick := range ticker.C {
		if tick.Day() > poll.today.Day() {
			poll.scoresToday = []Score{} // empty the list for today's scores
			poll.today = tick
		}

		updates := poll.client.GetUpdates(poll.lastId)
		if updates == nil {
			continue
		}

		for _, upd := range updates.Result {
			poll.handleUpdate(upd)
		}
		fmt.Printf("time: %v, updates: %#v\n", tick, updates)
	}
}

func (poll *Poller) handleUpdate(update api.Update) {
	if poll.lastId < update.Update_id {
		poll.lastId = update.Update_id
	}

	// we're interested only in messages
	msg := update.Message
	if msg == nil || msg.Text == "" {
		return // not message update, skip
	}

	if msg.Text == "/stat" || msg.Text == "/stat@l33t_count_bot" {
		go poll.handleStat(msg)
		return
	}

	if msg.Text == "/totals" || msg.Text == "/totals@l33t_count_bot" {
		go poll.handleTotals(msg)
		return
	}

	if msg.Text == "/setloc" || msg.Text == "/setloc@l33t_count_bot" {
		go poll.handleTzBinding(msg)
		return
	}

	if msg.Text == "/whoswhere" || msg.Text == "/whoswhere@l33t_count_bot" {
		go poll.handleWhosWhere(msg)
		return
	}

	fmt.Printf("Got message with text: %#v\n", msg.Text)

	// make sure it's l33t msg
	for _, regex := range LEET_REGEX {
		matches := regex.FindStringSubmatch(msg.Text)
		if matches == nil {
			continue
		}

		// someone has scored, kudos to him
		go poll.handleL33t(msg, matches[0], regex)
		return
	}
}

func (poll *Poller) handleTzBinding(msg *api.Message) {
	disablePreview := new(bool)
	*disablePreview = true
	answer := api.SendMessage{
		ChatId:                msg.Chat.Id,
		ParseMode:             "Markdown",
		ReplyToMessageId:      &msg.Message_id,
		DisableWebPagePreview: disablePreview}

	locMsg := msg.Reply_to_message
	if locMsg == nil || locMsg.Location == nil || locMsg.From.Id != msg.From.Id {
		// send fail report
		answer.Text = "Sorry, you should reply with `/setloc` to your message containing location"
		poll.client.SendObject(answer, ApiEndpoint+BotToken+SendMessagePath)
		return
	}

	// check for dummies - don't allow to change date more than 1 time in a week
	prevBind := poll.db.GetTzBinding(locMsg.From.Id)
	if prevBind != nil { // have previous record, check
		timePassed := time.Now().Sub(time.Unix(prevBind.Created, 0))
		if timePassed < time.Hour*24*7 {
			timeToGo := time.Hour*24*7 - timePassed
			answer.Text = "Sorry, you have already set your location earlier, wait at least " + timeToGo.String()
			poll.client.SendObject(answer, ApiEndpoint+BotToken+SendMessagePath)
			return
		}
	}

	zoneName := latlong.LookupZoneName(locMsg.Location.Latitude, locMsg.Location.Longitude)
	poll.db.SaveTzBinding(&TzBinding{PersonId: locMsg.From.Id, Tz: zoneName, Created: time.Now().Unix()})

	answer.Text = fmt.Sprintf("Set %s's timezone to %s", locMsg.From.First_name, zoneName)
	poll.client.SendObject(answer, ApiEndpoint+BotToken+SendMessagePath)
}

func (poll *Poller) handleStat(msg *api.Message) {
	total, highest, latest := poll.db.GetScores(msg.From.Id)
	report := fmt.Sprintf("Stats for %s:\n"+
		"  Total scored: %d points\n",
		msg.From.First_name,
		total)

	if highest != nil {
		report += fmt.Sprintf("  Highest score: %d points at %s\n",
			highest.Grade,
			highest.Time.Format("2006-Jan-02 15:04:05"))
	}

	if latest != nil {
		report += fmt.Sprintf("  Latest score: %d points at %s",
			latest.Grade,
			latest.Time.Format("2006-Jan-02 15:04:05"))
	}

	disablePreview := new(bool)
	*disablePreview = true
	request := api.SendMessage{
		Text:                  report,
		ChatId:                msg.Chat.Id,
		ParseMode:             "Markdown",
		ReplyToMessageId:      &msg.Message_id,
		DisableWebPagePreview: disablePreview}

	poll.client.SendObject(request, ApiEndpoint+BotToken+SendMessagePath)
}

func (poll *Poller) handleTotals(msg *api.Message) {
	totals := poll.db.GetTotals()

	disablePreview := new(bool)
	*disablePreview = true
	answer := api.SendMessage{
		ChatId:                msg.Chat.Id,
		ParseMode:             "Markdown",
		ReplyToMessageId:      &msg.Message_id,
		DisableWebPagePreview: disablePreview}

	// no stats available
	if len(totals) == 0 {
		answer.Text = "No stats available for this chat! Sorry..."
		poll.client.SendObject(answer, ApiEndpoint+BotToken+SendMessagePath)
		return
	}

	answer.Text = "Total stats for this chat members:\n"
	counter := 0
	for idx := range totals {
		member := poll.client.GetChatMember(msg.Chat.Id, totals[idx].PersonId)
		if member == nil { // no such person in this chat!
			continue
		}
		counter++
		answer.Text += fmt.Sprintf("  %d. %s - %d points\n", counter, member.User.First_name, totals[idx].TotalScored)
	}

	poll.client.SendObject(answer, ApiEndpoint+BotToken+SendMessagePath)
}

// Someone tried to make score, check him
func (poll *Poller) handleL33t(msg *api.Message, scored string, regex *regexp.Regexp) {
	currTime := time.Unix(msg.Date, 0)

	// convert date to user's requested timezone if exists
	userTz := poll.db.GetTzBinding(msg.From.Id)
	if userTz != nil {
		userLoc, err := time.LoadLocation(userTz.Tz)
		if err == nil {
			currTime = currTime.In(userLoc)
		} else {
			fmt.Printf("Couldn't convert location %s to Golang Location object", userTz.Tz)
		}
	}

	hmTimeStr := currTime.Format("1504")
	if !strings.HasPrefix(regex.String(), hmTimeStr) {
		return // score must match HHmm
	}
	//    yyyyMMddHHmmss
	timeStr := currTime.Format("20060102150405")
	match := regex.FindStringSubmatch(timeStr)
	if match == nil {
		return // not enough to be l33t!
	}

	// make sure person hasn't scored this one yet...
	for _, score := range poll.scoresToday {
		if score.PersonId == msg.From.Id && currTime.Sub(score.Time).Seconds() < 60 {
			fmt.Printf("Exploit user detected: %s, score time %s, message time %s\n",
				msg.From.First_name, score.Time.String(), currTime.String())
			return // diff between scores is less than minute? You dirty jackass!
		}
	}

	// someone scored, save it and report
	fmt.Printf("Timestring: %s, Message: %s Matched: %s\n", timeStr, scored, match)
	scoredSize := utf8.RuneCountInString(scored)    // e.g. we sent 111111 - wanted to score 11 seconds too, it'll be 6
	matchedSize := utf8.RuneCountInString(match[0]) // if time is 11:11:12 we'll get only 5
	totalMatched := uint8(min(scoredSize, matchedSize))

	score := Score{Message: msg, PersonId: msg.From.Id, Grade: totalMatched, Time: currTime}
	poll.db.SaveScore(&score) // save it to DB

	// get next minute start time and delete all messages from bot at that time
	secondsToGo := 60 - currTime.Second()
	if !poll.narrateAhead {
		fmt.Printf("Setting up narrate timer\n")
		go poll.printScores(secondsToGo)
		poll.narrateAhead = true
	}

	disablePreview := new(bool)
	*disablePreview = true
	report := fmt.Sprintf("%s is a l33t now, scored %d points, current time is %s",
		msg.From.First_name,
		totalMatched,
		currTime.String())

	request := api.SendMessage{
		Text:                  report,
		ChatId:                msg.Chat.Id,
		ParseMode:             "Markdown",
		ReplyToMessageId:      &msg.Message_id,
		DisableWebPagePreview: disablePreview}

	resp := poll.client.SendObject(request, ApiEndpoint+BotToken+SendMessagePath)
	if resp == nil {
		return // no response, should be logged
	}

	//io.Copy(os.Stdout, resp.Body)
	decoder := json.NewDecoder(resp.Body)
	var answer api.SendMessageResponse
	parseErr := decoder.Decode(&answer)
	if parseErr != nil {
		fmt.Println("Error parsing response ... " + parseErr.Error())
		return
	}

	score.BotMessage = answer.Result
	poll.scoresToday = append(poll.scoresToday, score) // save it to today's scores
}

// After all scores are taken, print total stats and delete all messages for scores
// that were printed in previous minute
func (poll *Poller) printScores(delay int) {
	time.Sleep(time.Duration(delay) * time.Second)

	fmt.Printf("[%s] Narrating total scores for previous minute.\n", time.Now().String())
	poll.narrateAhead = false

	// finishing move - delete all messages from bot and add summary
	lastMinuteScores := []Score{}
	var chatId int64
	for _, score := range poll.scoresToday {
		if time.Since(score.Time) < time.Second*60 {
			lastMinuteScores = append(lastMinuteScores, score)
			// TODO: group by chats!
			chatId = score.Message.Chat.Id
		}
	}

	report := fmt.Sprintf("Stats for last minute (%s):\n", time.Now().Add(-30*time.Second).Format("1504"))
	for _, score := range lastMinuteScores {
		poll.client.deleteMessage(score.BotMessage.Chat.Id, score.BotMessage.Message_id)
		report = report + fmt.Sprintf("  %s: *%d points* at %s\n",
			score.Message.From.First_name, score.Grade, score.Time.Format("15:04:05"))
	}

	request := api.SendMessage{
		Text:      report,
		ChatId:    chatId,
		ParseMode: "Markdown"}

	resp := poll.client.SendObject(request, ApiEndpoint+BotToken+SendMessagePath)
	if resp == nil {
		return // no response, should be logged
	}

	resp.Body.Close()
}

func (poll *Poller) handleWhosWhere(msg *api.Message) {

}
